//
//  AppDelegate.swift
//  LMSCore
//
//  Created by Conan Moriarty on 05/25/2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

import UIKit
import LMSCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }

}
