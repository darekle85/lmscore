# LMSCore

[![Build Status](https://www.bitrise.io/app/a5f01a1dbbaa5a6b.svg?token=RKZyw_0c575aaLsTvR4HdQ)](https://www.bitrise.io/app/a5f01a1dbbaa5a6b)

This Cocoapod is home to iOS code required by different projects. It will be the central location for business logic, class extensions, view and any other app-agnostic reusable code. 

## Installation

To include LMSCore in your project, add it to your Podfile (as of writing this README, the version of Cocoapods for LMS apps is 0.39.0) like so:

`pod 'LMSCore', :git => 'https://bitbucket.org/lm-solutions/lmscore.git'`


## Using LMSCore in apps

Swift: ```import LMSCore```

Obj-C: ```@import LMSCore;```
  
## Overriding Behavior

Let's say you want to use LMSCore, but you don't want custom behavior for the app you're working on.  You can inherit from the classes found in LMSCore and override the method(s) in question:

```swift 
import UIKit
import LMSCore

class CustomAppDateUtils: DateUtils {

  override class func monthAbbreviationDayYearStringFromDate(date: NSDate) -> String {
    return "String"
  }

}
```

# Contributing

## Workflow

The LMSCore project was created using Cocoapods v1.0.0.  You'll need to have this version installed locally to read the Podfile (otherwise `pod install` won't run successfully).  Contributing to LMSCore can be done in a fairly ad hoc manner, but please create a [Jira ticket](https://lifemap-solutions.atlassian.net/secure/RapidBoard.jspa?rapidView=32&projectKey=CIOS&view=planning) on the CIOS board to track the work you're doing and to have a Jira Ticket ID as a reference for your branch.  Let's say you're working on a project (e.g. LMSExampleProject) that needs certain convenience methods that will be useful elsewhere.  Follow these steps:

* Create a Jira Ticket on CIOS to track the work you will be doing
* Clone LMSCore
* Branch on local LMSCore (referencing Jira ticket)
* Branch on LMSExampleProject (referencing the Jira ticket that you're working on as part of this project)
* Change the Podfile in LMSExampleProject to point to the remote branch of LMSCore which has the new changes
* Make additions to LMS, add unit tests for any code added
* Submit for peer review on LMSCore
* Submit for peer review on LMSExampleProject (still pointing at the branch with the new changes)
* Upon successful peer review on LMSExampleProject, revert the Podfile back to `#pod 'LMSCore', :git => 'https://bitbucket.org/lm-solutions/lmscore.git'`

The reason we follow this workflow (as opposed to purely doing changes locally) is that it allows other developers on the team to build + run the app (LMSExampleProject) with the new additions and confirm that there are no regressions/the app functions as expected.  Any suggestions for how to improve this - let @conanmoriarty know. :)

## Criteria

#### Classes must be app agnostic
Good candidates are:

* Extensions/categories off UIKit
* Convenience classes
* Custom UI that can be repurposed

#### Classes must be app agnostic

Please don't include any code that ties a class to a single project.  

#### Unit Testing

Aim for 100% test coverage (where possible).  The majority of this codebase will no doubt be made up of extensions, convenience methods, custom UI etc. Unit tests help to make regressions less common and provide fast feedback during refactoring or changing logic in classes.  100% coverage as a goal is simply a way of covering your bases as opposed to be a strict directive.  That is because you can achieve 100% coverage of a method by adding a single simple test, but only adding the minimum amount of tests required to achieve good coverage (vs. adding multiple permutations of inputs and tests to assert the expected output arrives) is not advised.  You can check the coverage by completing the following steps:

1) Execute 'Test' on LMSCore target (CMD-U)
2) Show Report Navigator
3) Select LMSCore 'Test' report, click 'Coverage' tab

1)

![Screen Shot 2016-06-14 at 16.23.32.png](https://bitbucket.org/repo/oaozdG/images/36944786-Screen%20Shot%202016-06-14%20at%2016.23.32.png)

2)

![Screen Shot 2016-06-14 at 16.30.36.png](https://bitbucket.org/repo/oaozdG/images/2582303270-Screen%20Shot%202016-06-14%20at%2016.30.36.png)

3)

![Screen Shot 2016-06-14 at 16.30.52.png](https://bitbucket.org/repo/oaozdG/images/858394417-Screen%20Shot%202016-06-14%20at%2016.30.52.png)


Some good resources for Unit Testing:

* [Jon Reid's Quality Coding Blog](http://qualitycoding.org/)

* [Ray Wenderlich Tutorial on Mocking in Swift](https://www.raywenderlich.com/101306/unit-testing-tutorial-mocking-objects)

* [Uncle Bob's Three Rules of TDD](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)

* [Andrew Bancroft on 'legacy Swift'](http://www.andrewcbancroft.com/2014/12/10/dont-write-legacy-swift/)


Note: if you find yourself in a situation where you're not sure how to tackle a chunk of code then pair! It's tough sometimes to figure out how to write tests for a class or figure out how to refactor an interface into something that's more testable - but it's not impossible and it's often made easier when there's others there to help.  Just ask in Core iOS in Hipchat!

#### View based classes must be IBDesignable

This allows for Interface Builder customisation.  Please make properties IBInspectable where possible.  [Consult this guide](http://nshipster.com/ibinspectable-ibdesignable/) for tips. Do this where possible.  Also please use Auto-Layout unless setFrame is required (e.g. on CALayer instances)

#### Resources/Storyboards must be app agnostic 

It's ok to include png assets, storyboards etc. in the Assets folder, but please make sure that they don't make any reference to a specific app.

#### Classes must use LMS Prefix

This allows developers to quickly distinguish between app specific vs. company wide classes.  This rule doesn't apply to extensions or Obj-C categories, which would use `"<ClassName>+LMSAdditions"` (e.g. `NSDate+LMSAdditions.swift`)

#### Methods must be commented with Javadoc style

This explains the purpose of classes + methods as well as gives developers quick feedback on the params/input/output/description of a class.  I would suggest [installing this plugin](https://github.com/onevcat/VVDocumenter-Xcode) as it greatly speeds up the process.

## Versioning

LMSCore uses [semantic versioning](http://guides.rubygems.org/patterns/#semantic-versioning).  The master branch is tagged with the version which is also included in the .podspec file.  To update the repo, please use standard branch/commit/pull request and manually tag the release.  I (CDM) will look to add a CI step with our build machine.


## Authors

Conan Moriarty, conan.moriarty@lifemap-solutions.com

Robin McCollum, robin.mccollum@lifemap-solutions.com

My Lan Payson, mylan.payson@lifemap-solutions.com

David Coleman, david.coleman@lifemap-solutions.com

Dariusz Lesniak, dariusz.lesniak@lifemap-solutions.com

Pawel Kowalczyk, pawel.kowalczyk@lifemap-solutions.com