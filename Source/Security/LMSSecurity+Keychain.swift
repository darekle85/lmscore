//
//  Security+Keychain.swift
//  Pods
//
//  Created by Damian Rzeszot on 08/08/16.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

import Foundation
import Security


// swiftlint:disable variable_name
// swiftlint:disable valid_docs
// swiftlint:disable force_cast
// swiftlint:disable line_length



// Security CFStrings case for Swift Use
private let SecMatchLimit: String! = kSecMatchLimit as String
private let SecReturnData: String! = kSecReturnData as String
private let SecReturnAttributes: String! = kSecReturnAttributes as String
private let SecReturnPersistentRef: String! = kSecReturnPersistentRef as String
private let SecValueData: String! = kSecValueData as String
private let SecAttrAccessible: String! = kSecAttrAccessible as String
private let SecClass: String! = kSecClass as String
private let SecAttrService: String! = kSecAttrService as String
private let SecAttrGeneric: String! = kSecAttrGeneric as String
private let SecAttrAccount: String! = kSecAttrAccount as String
private let SecAttrAccessGroup: String! = kSecAttrAccessGroup as String

public typealias KeychainCompletionBlock = (_ success: Bool, _ errorString: String?) -> Void

private let CredentialsKey   = "LMSCoreLogin"
private let ServiceString    = "LMS"



extension LMSSecurity {


  // MARK: Keychain
  /**
   Persist credentials to keychain, typically called upon successful login or signup.

   - parameter username: Email Address for User in String format
   - parameter password: Password for User in String format
   - parameter completionBlock: (success:Bool, errorString: String?) Optional. ErrorString is a string that matches an OSStatus
   and returns a String (for the developer).
   */
  public class func addUserCredentialsToKeychain (_ username: String, password: String, completionBlock: KeychainCompletionBlock?) {

    var keychainQueryDictionary = self.setupKeychainQueryDictionary()

    keychainQueryDictionary[SecValueData] = password.data(using: String.Encoding.utf8) as AnyObject?
    keychainQueryDictionary[SecAttrAccessible] = kSecAttrAccessibleWhenUnlockedThisDeviceOnly
    keychainQueryDictionary[SecAttrAccount] = username.data(using: String.Encoding.utf8) as AnyObject?

    let status: OSStatus = SecItemAdd(keychainQueryDictionary as CFDictionary, nil)

    if status == errSecSuccess {
      completionBlock?(true, nil)
    } else if status == errSecDuplicateItem {
      self.updateUserCredentialsToKeychain(username, password: password, completionBlock: completionBlock)
    } else {
      self.handleError(status)
      completionBlock?(true, nil)
    }
  }

  /**
   Attempts to update username and password for user.  Generally called when credentials change successfully. This
   method can fail.

   - parameter username: Email Address for User in String format
   - parameter password: Password for User in String format
   - parameter completionBlock: (success:Bool, errorString: String?) Optional. ErrorString is a string that matches an OSStatus
   and returns a String (for the developer).
   */
  public class func updateUserCredentialsToKeychain (_ username: String, password: String, completionBlock: KeychainCompletionBlock?) {

    let keychainQueryDictionary = self.setupKeychainQueryDictionary()

    let usernameData = username.data(using: String.Encoding.utf8)!
    let passwordData = password.data(using: String.Encoding.utf8)!

    let updateDictionary = [SecValueData:passwordData, SecAttrAccount:usernameData]

    let status: OSStatus = SecItemUpdate(keychainQueryDictionary as CFDictionary, updateDictionary as CFDictionary)

    if status == errSecSuccess {
      completionBlock?(true, nil)
    } else {
      completionBlock?(false, self.handleError(status))
    }
  }

  /**
   Deletes Credentials from Keychain

   - parameter completionBlock: (success:Bool, errorString: String?) Optional. ErrorString is a string that matches an OSStatus
   and returns a String (for the developer).
   */
  public class func deleteUserCredentialsFromKeychain(_ completionBlock: KeychainCompletionBlock?) {
    let keychainQueryDictionary: [String:AnyObject] = self.setupKeychainQueryDictionary()

    let status: OSStatus = SecItemDelete(keychainQueryDictionary as CFDictionary)

    if status == errSecSuccess {
      completionBlock?(true, nil)
    } else {
      completionBlock?(true, self.handleError(status))
    }
  }

  /**
   Fetch username and password for user.

   - returns: Tuple with username String and password String optional, can return nil if no credentials are found
   */
  public class func getUserCredentialsFromKeychain() -> (username: String, password: String)? {

    var keychainQueryDictionary = self.setupKeychainQueryDictionary()
    var result: AnyObject?

    keychainQueryDictionary[SecReturnAttributes] = kCFBooleanTrue
    keychainQueryDictionary[SecReturnData] = kCFBooleanTrue

    let status = withUnsafeMutablePointer(to: &result) {
      SecItemCopyMatching(keychainQueryDictionary as CFDictionary, UnsafeMutablePointer($0))
    }

    if status == noErr {
      if let keychainObj = result, let accountValue = keychainObj[SecAttrAccount] as? Data {

        let usernameString = NSString(data: accountValue, encoding: String.Encoding.utf8.rawValue) as! String

        if let password = keychainObj["v_Data"] as? Data {
          let passwordString = NSString(data: password, encoding: String.Encoding.utf8.rawValue) as! String
          return (usernameString, passwordString)
        }
      }
    }

    return nil
  }

  /**
   Convenience method to build a valid Keychain Query Dictionary to carry out CRUD methods on Keychain.  Assumes
   Service Value is set using constant (i.e. we don't need to be setting anything other than LMS related
   credentials using this for now)

   - returns: Keychain Query Dictionary set up to create/update/delete Generic Passwords with a Identifier
   */
  class func setupKeychainQueryDictionary() -> [String:AnyObject] {
    var keychainQueryDictionary: [String:AnyObject] = [SecClass:kSecClassGenericPassword]
    keychainQueryDictionary[SecAttrService] = ServiceString as AnyObject?

    let encodedIdentifier: Data? = CredentialsKey.data(using: String.Encoding.utf8)
    keychainQueryDictionary[SecAttrGeneric] = encodedIdentifier as AnyObject?

    return keychainQueryDictionary
  }

  /**
   Takes OSStatus and returns meaningful error message + debug message or assertion for developer

   - parameter status: OSStatus - 32-bit result error code
   */
  class func handleError(_ status: OSStatus) -> String {
    if status == errSecParam {
      assert(status == errSecParam, "This error generally means you've got your Keychain Query dict set up incorrectly, make sure that you're using appropriate attributes and that any String/NSString values that you're passing in as arguments are NSData obj's formatted w/ NSUTF8Encoding.")
      return ("Function or operation not implemented.")
    } else if status == errSecUserCanceled {
      return ("User cancelled.")
    } else if status == errSecBadReq {
      return ("Bad request.")
    } else if status == errSecNotAvailable {
      return ("No keychain is available. ")
    } else if status == errSecItemNotFound {
      return ("Item not found.")
    } else if status == errSecInteractionNotAllowed {
      return ("Interaction not allowed.")
    } else if status == errSecDecode {
      assert(status == errSecDecode, "This error generally means you've got your Keychain Query dict set up incorrectly, make sure that you're using appropriate attributes and that any String/NSString values that you're passing in as arguments are NSData obj's formatted w/ NSUTF8Encoding.")
      return ("Unable to decode the provided data.")
    } else if status == errSecAuthFailed {
      return ("Authentication failed.")
    } else {
      return ("Keychain or TouchID error.")
    }
  }

}
