//
//  SecurityTableViewCell.swift
//  Pods
//
//  Created by Damian Rzeszot on 08/08/16.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable valid_docs


import Foundation
import LocalAuthentication


private let defaultTouchIDReason = "Please authenticate to proceed."


@available(iOS 8.0, *)
extension LMSSecurity {


  public enum SecurityTouchIDResponse {
    case success
    case failure(error: NSError)
  }


  /// - returns: True/False depending on whether TouchID is available on this device
  public class func canUseTouchID() -> Bool {
    let context = LAContext()
    return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
  }


  /// Handles TouchID authentication.  Successful Auth fires completion block returning login credentials stored in the keychain
  ///
  /// - parameter completion: Returns optional username/password tuple and optional error message
  public class func requestForTouchID(_ reason: String? = nil, completition block: @escaping (SecurityTouchIDResponse) -> Void) {

    let reason = reason ?? defaultTouchIDReason

    let context = LAContext()
    var error: NSError?

    guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
      block(.failure(error: error!))
      return
    }

    context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, error in
      main {
        if success {
          block(.success)
        } else if let error = error {
          block(.failure(error: error as NSError))
        } else {
          // cannot happen
        }
      }
    }
  }


  // MARK: -

  fileprivate class func main(_ block: @escaping (Void) -> Void) {
    DispatchQueue.main.async(execute: block)
  }

}
