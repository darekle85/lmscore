//
//  Dictionary.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 14.06.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable trailing_whitespace


import Foundation

public extension Dictionary {
  
  /**
   Union of self and the input dictionaries.
   
   - parameter dictionaries: Dictionaries to join
   - returns: Union of self and the input dictionaries
   */
  public func union (_ dictionaries: Dictionary...) -> Dictionary {
    var result = self
    
    dictionaries.forEach { (dictionary) -> Void in
      dictionary.forEach { (key, value) -> Void in
        _ = result.updateValue(value, forKey: key)
      }
    }
    
    return result
  }
}

/**
 Union operator |
 
 - parameter first:  first dictionary
 - parameter second: second dictionary
 
 - returns: Union of dictionaries first and second
 */
public func | <K: Hashable, V> (first: [K: V], second: [K: V]) -> [K: V] {
  return first.union(second)
}

/**
 Union operator +
 
 - parameter first:  first dictionary
 - parameter second: second dictionary
 
 - returns: Union of dictionaries first and second
 */
public func + <K: Hashable, V> (first: [K: V], second: [K: V]) -> [K: V] {
  return first.union(second)
}
