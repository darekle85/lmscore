//
//  LMSDateUtils.swift
//
//  Created by Robin McCollum on 12/28/14.
//  Copyright (c) 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable force_cast
// swiftlint:disable valid_docs
// swiftlint:disable control_statement


import Foundation

open class LMSDateUtils {

  // MARK: NSString/Date formatting

  /**
   Formats raw date string from RSS feed into a "time ago" format if within 24h of current date

   - parameter string: Date String

   - returns: Time Ago date string (e.g. "Just Now", "5m").  Returns "Unknown" if date can't be unwrapped (Date formatting error)
   */
  open class func stringFromRSSDate(_ string: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZ"

    if let date = dateFormatter.date(from: string) {
      return timeSinceEventOrStringIfOverOneDayAgo(date, otherDate: Date.init())
    }

    return "Unknown"
  }

  /**
   Formats raw date string from Twitter API into a "time ago" format if within 24h of current date

   - parameter string: Twitter Date String

   - returns: Time Ago date string (e.g. "Just Now", "5m").  Returns "Unknown" if date can't be unwrapped (Date formatting error)
   */
  open class func stringFromTwitterDate(_ string: String) -> String {
    let dateFormatter = DateFormatter()

    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"

    if let date = dateFormatter.date(from: string) {
      return timeSinceEventOrStringIfOverOneDayAgo(date, otherDate: Date.init())
    }

    return "Unknown"
  }

  /**
   Compares dates and returns a string in a "time ago" format describing the difference between the two dates if within a 24h period.

   - returns: Time ago string if within 24h period or Date String (MMM dd)
   */
  open class func timeSinceEventOrStringIfOverOneDayAgo(_ date: Date, otherDate: Date) -> String {

    if (date == otherDate) {
      return "Just now"
    }

    let earlierDate = (date as NSDate).earlierDate(otherDate)
    let laterDate = (date as NSDate).laterDate(otherDate)

    let difference: DateComponents = self.difference(betweenDate: earlierDate, laterDate: laterDate)

    if (difference.hour! < 24) {
      if (difference.hour! >= 1) {
        return  "\(difference.hour)h"
      } else if (difference.minute! >= 1) {
        return "\(difference.minute)m"
      } else {
        return "Just now"
      }
    } else {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "MMM dd"
      return dateFormatter.string(from: earlierDate)
    }
  }

  /**
   Formats Date into 'MMM dd, YYYY'
   */
  open class func monthAbbreviationDayYearStringFromDate(_ date: Date, timeZone: TimeZone = TimeZone.current) -> String {
    let dateOfBirthFormatter = DateFormatter()
    dateOfBirthFormatter.timeZone = timeZone
    dateOfBirthFormatter.dateFormat = "MMM dd, YYYY"
    return dateOfBirthFormatter.string(from: date)
  }

  /**
   Returns String from Date

   - parameter timeZone: TimeZone for date (defaults to NSTimeZone.defaultTimeZone())
   */
  open class func stringFromDate(_ date: Date, timeZone: TimeZone = TimeZone.current) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

    dateFormatter.timeZone = timeZone
    return dateFormatter.string(from: date)
  }

  /**
   Returns String from Date

   - returns: String in "yyyy-MM-dd'T'HH:mm:ssZ" format
   */

  /**
   Returns short date String from Date

   - returns: String in "yyyy-MM-dd" format
   */
  open class func stringFromShortDate(_ date: Date, timeZone: TimeZone = TimeZone.current) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd"
    dateFormatter.timeZone = timeZone
    return dateFormatter.string(from: date)
  }

  /**
   Returns String from Date Components

   - parameter components: NSDateComponents instance with Hours/Minutes/Seconds set

   - returns: String in "HH:mm:ss"
   */
  open class func stringFromDateComponents(_ components: DateComponents) -> String {
    let calendar: Calendar = Calendar.current
    let date: Date = calendar.date(from: components)!

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    return dateFormatter.string(from: date)
  }

  /**
   Returns time String for Date

   - returns: String with time + "AM/PM" appended (e.g. "11 pm")
   */
  open class func timeFromDate(_ date: Date) -> String {
    let formatter: DateFormatter = DateFormatter()
    formatter.timeStyle = DateFormatter.Style.short
    formatter.amSymbol = "am"
    formatter.pmSymbol = "pm"
    formatter.dateStyle = DateFormatter.Style.none
    return  formatter.string(from: date)
  }

  /**
   Returns Date for String

   - parameter string: String in "yyyy-MM-dd'T'HH:mm:ssZ" format
   */
  open class func dateFromString(_ string: String, timeZone: TimeZone = TimeZone.current) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatter.timeZone = timeZone
    return dateFormatter.date(from: string)
  }

  /**
   Returns Date for RFC1123 String

   - parameter string: String in "EEE, dd MMM yyyy HH:mm:ss z" format
   */
  open class func dateFromRFC1123(_ string: String, timeZone: TimeZone = TimeZone.current) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
    dateFormatter.timeZone = timeZone
    return dateFormatter.date(from: string)
  }

  /**
   Returns Date for short String format

   - parameter string: "yyyy-MM-dd"
   */
  open class func dateFromShortString(_ string: String, timeZone: TimeZone = TimeZone.current) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd"
    dateFormatter.timeZone = timeZone
    return dateFormatter.date(from: string)
  }


  // MARK: NSDateComponents Convenience
  /**
   Compares two dates

   - returns: NSDateComponents for hour/minute/second difference between two dates
   */
  open class func difference(betweenDate earlierDate: Date, laterDate: Date) -> DateComponents {
    let flags: NSCalendar.Unit = [.hour, .minute, .second]
    let calendar = Calendar.current
    let difference = (calendar as NSCalendar).components(flags, from: earlierDate, to: laterDate, options: NSCalendar.Options(rawValue: 0))
    return difference
  }

  /**
   Returns String from NSDateComponents
   */
  open class func dateComponentsFromString(_ string: String) -> DateComponents {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    let date: Date = dateFormatter.date(from: string)!
    let calendar: Calendar = Calendar.current
    return  (calendar as NSCalendar).components([NSCalendar.Unit.hour, NSCalendar.Unit.minute, NSCalendar.Unit.second], from: date)
  }

  // MARK: JSON Parsing
  open class func dateFromStringHelper(_ string: AnyObject!) -> AnyObject! {
    return LMSDateUtils.dateFromString(string as! String)! as AnyObject!
  }

  open class func stringFromDateHelper(_ date: AnyObject!) -> AnyObject! {
    let dateValue = date as! Date
    return LMSDateUtils.stringFromDate(dateValue) as AnyObject!
  }

  open class func dateFromShortStringHelper(_ string: AnyObject!) -> AnyObject! {
    return LMSDateUtils.dateFromShortString(string as! String)! as AnyObject!
  }

  open class func stringFromShortDateHelper(_ date: AnyObject!) -> AnyObject! {
    return LMSDateUtils.stringFromShortDate(date as! Date) as AnyObject!
  }

  open class func dateComponentsFromStringHelper(_ string: AnyObject!) -> AnyObject! {
    return LMSDateUtils.dateComponentsFromString(string as! String) as AnyObject!
  }

  open class func stringFromDateComponentsHelper(_ date: AnyObject!) -> AnyObject! {
    return LMSDateUtils.stringFromDateComponents(date as! DateComponents) as AnyObject!
  }

}
