//
//  NSDate+LMSAdditions.swift
//  Pods
//
//  Created by Conan Moriarty on 14/06/2016.
//
//

// swiftlint:disable trailing_whitespace
// swiftlint:disable valid_docs
// swiftlint:disable function_parameter_count


import Foundation

extension Date {

  // MARK: Convenience

  /**
   Inits Date using Year, Month & Day using NSDateComponents
   */
  public static func dateShortHand(_ year: Int, month: Int, day: Int, timeZone: TimeZone = TimeZone.current) -> Date {
    var components = DateComponents()
    components.day = day
    components.month = month
    components.year = year
    var calendar = Calendar.current
    calendar.timeZone = timeZone
    return calendar.date(from: components)!
  }

  /**
   Inits Date using Year, Month, Day, Hour and Minute using NSDateComponents
   */
  public static func dateShortHand(_ year: Int, month: Int, day: Int, hour: Int, minute: Int, timeZone: TimeZone = TimeZone.current) -> Date {
    var components = DateComponents()
    components.day = day
    components.month = month
    components.year = year
    components.hour = hour
    components.minute = minute
    var calendar = Calendar.current
    calendar.timeZone = timeZone
    return calendar.date(from: components)!
  }

  /**
   Inits Date using Year, Month, Day, Hour, Minute and Second using NSDateComponents
   */
  public static func dateShortHand(_ year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int, timeZone: TimeZone = TimeZone.current) -> Date {
    var components = DateComponents()
    components.day = day
    components.month = month
    components.year = year
    components.hour = hour
    components.minute = minute
    components.second = second
    var calendar = Calendar.current
    calendar.timeZone = timeZone
    return calendar.date(from: components)!
  }

  // MARK: NSDate Convenience methods

  /**
   - returns: Beginning hour/minute/second of Date
   */
  public func floorDate() -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .year, .month, .day]
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components(flags, from: self)
    let dateOnly = calendar.date(from: components)!
    return dateOnly
  }

  /**
   - returns: Beginning hour/minute/second of week
   */
  public func floorDateWeek() -> Date {
    return getPreviousDayInWeek(1)
  }

  /**
   - returns: Beginning hour/minute/second of month
   */
  public func floorDateMonth() -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .era, .year, .month]
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components(flags, from: self)
    let dateOnly = calendar.date(from: components)!
    return dateOnly
  }

  /**
   - returns: Ending hour/minute/second of day
   */
  public func ceilDate() -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .era, .year, .month, .day]
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components(flags, from: self)
    let dateOnly = calendar.date(from: components)!
    var timeComponent = DateComponents()
    timeComponent.day = 1
    let nextDate = (calendar as NSCalendar).date(byAdding: timeComponent, to: dateOnly, options: [])!
    var secondComponent = DateComponents()
    secondComponent.second = -1
    return (calendar as NSCalendar).date(byAdding: secondComponent, to: nextDate, options: [])!
  }

  /**
   - returns: Ending hour/minute/second of week
   */
  public func ceilDateWeek() -> Date {
    return getNextDayInWeek(1)
  }

  /**
   - returns: Ending hour/minute/second of month
   */
  public func ceilDateMonth() -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .era, .year, .month]
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components(flags, from: self)
    let dateOnly = calendar.date(from: components)!
    var timeComponent = DateComponents()
    timeComponent.month = 1
    let nextDate = (calendar as NSCalendar).date(byAdding: timeComponent, to: dateOnly, options: [])!
    var secondComponent = DateComponents()
    secondComponent.second = -1
    return (calendar as NSCalendar).date(byAdding: secondComponent, to: nextDate, options: [])!
  }

  /**
   Returns previous day of the week within 7 day range of date entered

   - parameter day: Int between 0-6
   */
  public func getPreviousDayInWeek(_ day: Int) -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .era, .yearForWeekOfYear, .weekOfYear]
    let calendar = Calendar.current
    var components = (calendar as NSCalendar).components(flags, from: self)
    components.weekday = day
    let dateOnly = calendar.date(from: components)!
    return dateOnly
  }

  /**
   Returns next day of the week within 7 day range of date entered

   - parameter day: Int between 0-6
   */
  public func getNextDayInWeek(_ day: Int) -> Date {
    let flags: NSCalendar.Unit = [.timeZone, .era, .yearForWeekOfYear, .weekOfYear]
    let calendar = Calendar.current
    var components = (calendar as NSCalendar).components(flags, from: self)
    components.weekday = day
    let dateOnly = calendar.date(from: components)!
    var timeComponent = DateComponents()
    timeComponent.weekOfYear = 1
    let nextDate = (calendar as NSCalendar).date(byAdding: timeComponent, to: dateOnly, options: [])!
    var secondComponent = DateComponents()
    secondComponent.second = -1
    return (calendar as NSCalendar).date(byAdding: secondComponent, to: nextDate, options: [])!
  }

  /**
   Adds NSComponents to Date
   */
  public func dateByAddingComponents(_ comps: DateComponents, options opts: NSCalendar.Options) -> Date? {
    let calendar = Calendar.current
    return (calendar as NSCalendar).date(byAdding: comps, to: self, options: opts)
  }

  /**
   Subtracts NSComponents to Date
   */
  public func dateBySubtractingComponents(_ comps: DateComponents, options opts: NSCalendar.Options) -> Date? {
    var inverseComps: DateComponents = DateComponents()

    (inverseComps as NSDateComponents).calendar = (comps as NSDateComponents).calendar
    (inverseComps as NSDateComponents).timeZone = (comps as NSDateComponents).timeZone

    inverseComps.year = comps.year! == Int.max ? Int.max : -comps.year
    inverseComps.month = comps.month! == Int.max ? Int.max : -comps.month
    inverseComps.day = comps.day! == Int.max ? Int.max : -comps.day
    inverseComps.hour = comps.hour! == Int.max ? Int.max : -comps.hour
    inverseComps.minute = comps.minute! == Int.max ? Int.max : -comps.minute
    inverseComps.second = comps.second! == Int.max ? Int.max : -comps.second
    inverseComps.nanosecond = comps.nanosecond! == Int.max ? Int.max : -comps.nanosecond
    inverseComps.weekday = comps.weekday! == Int.max ? Int.max : -comps.weekday
    inverseComps.weekdayOrdinal = comps.weekdayOrdinal! == Int.max ? Int.max : -comps.weekdayOrdinal
    inverseComps.quarter = comps.quarter! == Int.max ? Int.max : -comps.quarter
    inverseComps.weekOfMonth = comps.weekOfMonth! == Int.max ? Int.max : -comps.weekOfMonth
    inverseComps.weekOfYear = comps.weekOfYear! == Int.max ? Int.max : -comps.weekOfYear
    inverseComps.yearForWeekOfYear = comps.yearForWeekOfYear! == Int.max ? Int.max : -comps.yearForWeekOfYear
    inverseComps.isLeapMonth = comps.isLeapMonth

    return dateByAddingComponents(inverseComps, options: opts)
  }

  /**
   Checks Two Dates occurred on the same day using NSDateComponents
   */
  public func isSameDay(_ day2: Date) -> Bool {
    let flags: NSCalendar.Unit = [.year, .month, .day]
    let calendar = Calendar.current
    let components1 = (calendar as NSCalendar).components(flags, from: self)
    let date1: Date = calendar.date(from: components1)!

    let components2 = (calendar as NSCalendar).components(flags, from: day2)
    let date2: Date = calendar.date(from: components2)!
    return date1.timeIntervalSince(date2) == 0
  }

  /**
   Adjusts NSDate using NSTimeZone
   */
  public func adjustDateUsingTimeZone(_ timeZone: TimeZone) -> Date {
    let strDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

    let dateFormatter = DateFormatter.init()
    dateFormatter.dateFormat = strDateFormat
    dateFormatter.timeZone = TimeZone.current

    let dateFormatterForTimeZone = DateFormatter.init()
    dateFormatterForTimeZone.dateFormat = strDateFormat
    dateFormatterForTimeZone.timeZone = timeZone
    
    return dateFormatterForTimeZone.date(from: dateFormatter.string(from: self))!
  }
}
