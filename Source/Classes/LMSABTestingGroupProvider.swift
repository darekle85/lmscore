//
//  LMSABTestingGroupProvider.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 01.07.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable trailing_whitespace
// swiftlint:disable valid_docs


import Foundation

public enum LMSABTestingGroup {
  case a
  case b
}

public protocol LMSABTestingGroupProviding {
  
  var testGroup: LMSABTestingGroup { get }
}

/**
 Provides user assignment to the 'AB Testing' group ('A' or 'B')
 based on the provided uuid ('unique device identifier' by default).
 
 In the heart of the assignment logic, the test group is genereated
 based on the group input (number between 0 and 99):
 *  0...49 falls into 'A' group
 * 50...99 falls into 'B' group
 Group input is generated based on the crc32 checksum of the uuid.
 */
public struct LMSABTestingGroupProvider: LMSABTestingGroupProviding {
  
  fileprivate let groupInputKey = "ABTestingGroupInput"
  fileprivate let groupARange =  0...49
  fileprivate let groupBRange = 50...99
  /**
   Returns 'AB Testing' group input (number between 0 and 99).
   On the first call, the group number is generated
   and stored to NSUserDefaults. The stored value is returned
   in the subsequent calls of this property.
   */
  internal var testGroupInput: Int {
    if let groupInput = self.userDefaults.object(forKey: self.groupInputKey) as? Int {
      return groupInput
    }
    let groupInput = self.generateGroupInput(self.uuid)
    self.userDefaults.set(groupInput, forKey:self.groupInputKey)
    self.userDefaults.synchronize()
    return groupInput
  }
  public var uuid: UUID? = UUID()
  public var userDefaults = UserDefaults.standard
  
  // MARK: public methods
  
  public init() {}
  public init(uuid: UUID?, userDefaults: UserDefaults) {
    self.uuid = uuid
    self.userDefaults = userDefaults
  }
  
  /**
   Returns 'AB Testing' group ('A' or 'B') based on the uuid.
   */
  public var testGroup: LMSABTestingGroup {
    if self.groupBRange.contains(self.testGroupInput) {
      return .b
    }
    return .a
  }
  
  // MARK: private methods
  
  /**
   Generates number between 0 and 99 based on the crc32 of the uuid.
   */
  internal func generateGroupInput(_ uuid: UUID?) -> Int {
    var groupInput = 0
    guard let crc32 = uuid?.uuidString.data(using: String.Encoding.utf8)?.crc32() else {
      return groupInput
    }
    groupInput = Int(crc32 % 100)
    
    return groupInput
  }
}
