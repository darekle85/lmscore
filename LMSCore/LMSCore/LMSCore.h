//
//  LMSCore.h
//  LMSCore
//
//  Created by Damian Rzeszot on 09/08/16.
//  Copyright © 2016 Damian Rzeszot. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for LMSCore.
FOUNDATION_EXPORT double LMSCoreVersionNumber;

//! Project version string for LMSCore.
FOUNDATION_EXPORT const unsigned char LMSCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LMSCore/PublicHeader.h>


