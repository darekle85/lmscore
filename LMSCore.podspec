#
# Be sure to run `pod lib lint LMSCore.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LMSCore'
  s.version          = '0.4.7'
  s.summary          = 'Reusable LifeMap Solutions classes.'

  s.description      = <<-DESC
  TODO: Add long description of the pod here.
  DESC

  s.homepage          = 'https://bitbucket.org/lm-solutions/lmscore/overview'
  s.license           = { type: 'MIT', file: 'LICENSE' }

  s.authors           = {
    "David Coleman" => "david.coleman@lifemap-solutions.com",
    "Pawel Kowalczyk"=> "pawel.kowalczyk@lifemap-solutions.com",
    "Dariusz Lesniak"=> "dariusz.lesniak@lifemap-solutions.com",
    "Robin McCollum"=> "robin.mccollum@lifemap-solutions.com",
    "Conan Moriarty"=> "conan.moriarty@lifemap-solutions.com",
    "My Lan Payson"=> "mylan.payson@lifemap-solutions.com",
    "Damian Rzeszot"=> "damian.rzeszot@lifemap-solutions.com",
  }

  s.source            = { git: 'https://bitbucket.org/darekle85/lmscore.git', tag: s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.frameworks = 'Foundation'
  s.source_files = 'Source/Classes/**/*'

  s.subspec 'Security' do |t|
    t.frameworks = ['Foundation', 'LocalAuthentication', 'Security']
    t.source_files = 'Source/Security/*.swift'
  end
end
