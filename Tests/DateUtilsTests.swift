//
//  LMSDateUtilsTests.swift
//  LMSCore
//
//  Created by Conan Moriarty on 07/06/2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable type_body_length


import UIKit
import XCTest
@testable import LMSCore

class LMSDateUtilsTests: XCTestCase {

  var twitterDateFormatter = DateFormatter()
  var rssDateFormatter = DateFormatter()

  override func setUp() {
    super.setUp()
    NSTimeZone.setDefaultTimeZone(TimeZone.init(abbreviation: "GMT")!)

    twitterDateFormatter.locale = Locale(identifier: "en_US_POSIX")
    twitterDateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
    rssDateFormatter.locale = Locale(identifier: "en_US_POSIX")
    rssDateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZ"
  }

  // MARK: Month Abbreviation
  func testDateReturnsFormattedMonthAbbreviation() {
    XCTAssert(LMSDateUtils.monthAbbreviationDayYearStringFromDate(Date.dateShortHand(1999, month: 1, day: 1, hour: 12, minute: 0)) == "Jan 01, 1999")
  }

  func testMiddayFloorsToValidDate() {
    let midday = Date.dateShortHand(2016, month: 1, day: 2, hour: 23, minute: 33)

    XCTAssert(midday.floorDate() == Date.dateShortHand(2016, month: 1, day: 2, hour: 0, minute: 0, second: 0))
  }

  // MARK: NSDate -> String Tests
  func testSameDateReturnsJustNowString() {
    let dateComponents = DateComponents.init()

    let nowDate = Date.init()

    let fiveMinsAgo = (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: nowDate, options: NSCalendar.Options(rawValue: 0))!
    let twitterStringForDOB = LMSDateUtils.timeSinceEventOrStringIfOverOneDayAgo(fiveMinsAgo, otherDate: nowDate)

    XCTAssert(twitterStringForDOB == "Just now")

  }

  func test5MinsAgoReturns5MinsAgoString() {

    let elevenPM = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let fiveToElevenPM = elevenPM.addingTimeInterval(-(60 * 5))

    let twitterStringForDOB = LMSDateUtils.timeSinceEventOrStringIfOverOneDayAgo(fiveToElevenPM, otherDate: elevenPM)

    XCTAssert(twitterStringForDOB == "5m")
  }

  func test1MinsAgoReturns1MinsAgoString() {

    let elevenPM = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let fiveToElevenPM = elevenPM.addingTimeInterval(-(60 * 1))

    let twitterStringForDOB = LMSDateUtils.timeSinceEventOrStringIfOverOneDayAgo(fiveToElevenPM, otherDate: elevenPM)

    XCTAssert(twitterStringForDOB == "1m")
  }

  func test59SecsAgoReturns59SecsAgoString() {

    let elevenPM = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let fiveToElevenPM = elevenPM.addingTimeInterval(-(59))

    let twitterStringForDOB = LMSDateUtils.timeSinceEventOrStringIfOverOneDayAgo(fiveToElevenPM, otherDate: elevenPM)

    XCTAssert(twitterStringForDOB == "Just now")
  }

  func test60MinsAgoReturns1HourAgoStringUsingTwitterDate() {
    var dateComponents = DateComponents.init()
    dateComponents.minute = -60

    let fiveMinsAgo = (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: Date.init(), options: NSCalendar.Options(rawValue: 0))

    let twitterDateString = twitterDateFormatter.string(from: fiveMinsAgo!)
    let twitterStringForDOB = LMSDateUtils.stringFromTwitterDate(twitterDateString)

    XCTAssert(twitterStringForDOB == "1h")

  }

  func test23HoursAgoReturns23HoursAgoString() {
    var dateComponents = DateComponents.init()
    dateComponents.hour = -23

    let fiveMinsAgo = (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: Date.init(), options: NSCalendar.Options(rawValue: 0))

    let twitterDateString = twitterDateFormatter.string(from: fiveMinsAgo!)
    let twitterStringForDOB = LMSDateUtils.stringFromTwitterDate(twitterDateString)

    XCTAssert(twitterStringForDOB == "23h")
  }

  func test24HoursAgoReturnsFullDateString() {
    var dateComponents = DateComponents.init()
    dateComponents.hour = -24

    let elevenPM = Date.dateShortHand(2016, month: 01, day: 02, hour: 23, minute: 0)

    let fiveMinsAgo = (Calendar.current as NSCalendar).date(byAdding: dateComponents, to: elevenPM, options: NSCalendar.Options(rawValue: 0))

    let twitterDateString = twitterDateFormatter.string(from: fiveMinsAgo!)
    let twitterStringForDOB = LMSDateUtils.stringFromTwitterDate(twitterDateString)

    XCTAssert(twitterStringForDOB == "Jan 01")
  }

  func testStringFromRSSDateReturnsJustNowString() {
    let now = Date.init()
    let dateString = rssDateFormatter.string(from: now)

    XCTAssert(LMSDateUtils.stringFromRSSDate(dateString) == "Just now")
  }

  func testStringFromRSSDateReturns5MinsAgoString() {
    let fiveMinsAgo = Date.init().addingTimeInterval(-(60 * 5))
    let fiveMinsAgoString = rssDateFormatter.string(from: fiveMinsAgo)

    XCTAssert(LMSDateUtils.stringFromRSSDate(fiveMinsAgoString) == "5m")
  }

  func testStringFromRSSDateReturns1HourAgoString() {
    let oneHourAgo = Date.init().addingTimeInterval(-(60 * 60))
    let oneHourAgoString = rssDateFormatter.string(from: oneHourAgo)

    XCTAssert(LMSDateUtils.stringFromRSSDate(oneHourAgoString) == "1h")

  }

  func testStringFromRSSDateReturnsFullDateStringString() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let janFirstString = rssDateFormatter.string(from: janFirstDate)

    XCTAssert(LMSDateUtils.stringFromRSSDate(janFirstString) == "Jan 01")
  }

  func testStringFromRSSDateReturnsUnknownForInvalidlyFormattedDateString() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/mm/yyyy"
    let janFirstString = dateFormatter.string(from: janFirstDate)

    XCTAssert(LMSDateUtils.stringFromRSSDate(janFirstString) == "Unknown")
  }

  func testStringFromTwitterDateReturnsJustNowString() {
    let now = Date.init()
    let dateString = twitterDateFormatter.string(from: now)

    XCTAssert(LMSDateUtils.stringFromTwitterDate(dateString) == "Just now")
  }

  func testStringFromTwitterDateReturns5MinsAgoString() {
    let fiveMinsAgo = Date.init().addingTimeInterval(-(60 * 5))
    let fiveMinsAgoString = twitterDateFormatter.string(from: fiveMinsAgo)

    XCTAssert(LMSDateUtils.stringFromTwitterDate(fiveMinsAgoString) == "5m")
  }

  func testStringFromTwitterDateReturns1HourAgoString() {
    let oneHourAgo = Date.init().addingTimeInterval(-(60 * 60))
    let oneHourAgoString = twitterDateFormatter.string(from: oneHourAgo)

    XCTAssert(LMSDateUtils.stringFromTwitterDate(oneHourAgoString) == "1h")

  }

  func testStringFromTwitterDateReturnsFullDateStringString() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let janFirstString = twitterDateFormatter.string(from: janFirstDate)
    XCTAssert(LMSDateUtils.stringFromTwitterDate(janFirstString) == "Jan 01")
  }

  func testStringFromTwitterDateReturnsUnknownForInvalidlyFormattedDateString() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 23, minute: 0)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/mm/yyyy"
    let janFirstString = dateFormatter.string(from: janFirstDate)
    XCTAssert(LMSDateUtils.stringFromTwitterDate(janFirstString) == "Unknown")
  }

  func testStringFromDate() {
    let janFirst = Date.dateShortHand(2016, month: 01, day: 01, hour: 12, minute: 0)
    XCTAssert(LMSDateUtils.stringFromDate(janFirst) == "2016-01-01T12:00:00+0000")
  }

  func testStringFromShortDate() {
    let janFirst = Date.dateShortHand(2016, month: 01, day: 01, hour: 12, minute: 0)
    XCTAssert(LMSDateUtils.stringFromShortDate(janFirst) == "2016-01-01")
  }

  func testStringFromDateComponents() {
    var components = DateComponents()
    components.hour = 12
    components.minute = 0
    components.second = 0

    XCTAssert(LMSDateUtils.stringFromDateComponents(components) == "12:00:00")
  }

  func testFourPMReturnsValidTimeFromDate() {
    var components = DateComponents()
    components.hour = 16
    components.minute = 0
    components.second = 0
    let calendar: Calendar = Calendar.current
    let date: Date = calendar.date(from: components)!

    XCTAssert(LMSDateUtils.timeFromDate(date) == "4:00 pm")
  }

  func testElevenAMReturnsValidTimeFromDate() {
    var components = DateComponents()
    components.hour = 11
    components.minute = 0
    components.second = 0
    let calendar: Calendar = Calendar.current
    let date: Date = calendar.date(from: components)!

    XCTAssert(LMSDateUtils.timeFromDate(date) == "11:00 am")
  }

  func testSubtracting1SecFromDateReturns1SecondAgo() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 0, minute: 0)
    var components = DateComponents()
    components.second = 1

    XCTAssert(Date.dateShortHand(2015, month: 12, day: 31, hour: 23, minute: 59, second: 59) == janFirstDate.dateBySubtractingComponents(components, options: []))
  }

  func testSubtracting1MinFromDateReturns1MinsAgo() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 0, minute: 0)
    var components = DateComponents()
    components.minute = 1

    XCTAssert(Date.dateShortHand(2015, month: 12, day: 31, hour: 23, minute: 59) == janFirstDate.dateBySubtractingComponents(components, options: []))
  }

  func testSubtracting1MonthFromDateReturns1MonthAgo() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 0, minute: 0)
    var components = DateComponents()
    components.month  = 1

    XCTAssert(Date.dateShortHand(2015, month: 12, day: 01, hour: 0, minute: 0) == janFirstDate.dateBySubtractingComponents(components, options: []))
  }

  func testSubtracting1YearFromDateReturns1YearAgo() {
    let janFirstDate = Date.dateShortHand(2016, month: 01, day: 01, hour: 0, minute: 0)
    var components = DateComponents()
    components.year = 1

    XCTAssert(Date.dateShortHand(2015, month: 01, day: 01, hour: 0, minute: 0) == janFirstDate.dateBySubtractingComponents(components, options: []))
  }

  func testTodayIsSameDay() {
    XCTAssert(Date.init().isSameDay(Date.init()))
  }

  func differentDayIsNotSameDay() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 0, minute: 0, second: 0)
    let decThirtyFirst = Date.dateShortHand(2015, month: 12, day: 31, hour: 0, minute: 0, second: 0)
    XCTAssert(!janFirst.isSameDay(decThirtyFirst))
  }

  func sameDayDifferentYearIsNotSameDay() {
    let janFirst2016 = Date.dateShortHand(2016, month: 1, day: 1, hour: 0, minute: 0, second: 0)
    let janFirst2015 = Date.dateShortHand(2015, month: 1, day: 1, hour: 0, minute: 0, second: 0)
    XCTAssert(!janFirst2016.isSameDay(janFirst2015))
  }

  func testCeilDate() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 1, minute: 0, second: 0)

    XCTAssert(janFirst.ceilDate() == Date.dateShortHand(2016, month: 1, day: 1, hour: 23, minute: 59, second: 59))
  }

  func testCeilDateWeek() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 1, minute: 0, second: 0)

    XCTAssert(janFirst.ceilDateWeek() == Date.dateShortHand(2016, month: 1, day: 2, hour: 23, minute: 59, second: 59))
  }

  func testCeilDateMonth() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 1, minute: 0, second: 0)

    XCTAssert(janFirst.ceilDateMonth() == Date.dateShortHand(2016, month: 1, day: 31, hour: 23, minute: 59, second: 59))
  }

  func testFloorDateWeek() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 1, minute: 0, second: 0)

    XCTAssert(janFirst.floorDateWeek() == Date.dateShortHand(2015, month: 12, day: 27))
  }

  func testFloorDateMonth() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 15, hour: 21, minute: 0, second: 0)

    XCTAssert(janFirst.floorDateMonth() == Date.dateShortHand(2016, month: 1, day: 1))
  }

  func testPreviousDay3InFirstWeek2016isTuesday() {
    let janFirst = Date.dateShortHand(2016, month: 1, day: 1, hour: 1, minute: 0, second: 0)

    XCTAssert(janFirst.getPreviousDayInWeek(3) == Date.dateShortHand(2015, month: 12, day: 29))
  }

  func testInvalidDateFromStringReturnsNil() {
    XCTAssert(LMSDateUtils.dateFromString("not a date") == nil)
  }

  func testValidDateFromString() {
    let dateOne = Date.dateShortHand(2016, month: 6, day: 15, hour: 16, minute: 50, second: 08)
    let dateTwo = LMSDateUtils.dateFromString("2016-06-15T16:50:08+0000")

    XCTAssert(dateOne == dateTwo)
  }

  func testDateFromRCF1123String() {

    let dateOne = Date.dateShortHand(2016, month: 6, day: 15, hour: 16, minute: 57, second: 49)
    let dateTwo = LMSDateUtils.dateFromRFC1123("Wed, 15 Jun 2016 16:57:49 GMT")

    XCTAssert(dateOne == dateTwo)
  }

  func testInvalidDateStringFromRCF1123StringReturnsNil() {
    let invalidDate = LMSDateUtils.dateFromRFC1123("invalid date")

    XCTAssert(invalidDate == nil)
  }

  func testDateFromShortString() {
    let date = LMSDateUtils.dateFromShortString("2016-04-06")

    XCTAssert(date == Date.dateShortHand(2016, month: 4, day: 06))
  }

  func testDateComponentsFromString() {
    let timeString = "23:05:01"
    var components = DateComponents()
    components.hour = 23
    components.minute = 5
    components.second = 1

    XCTAssert(LMSDateUtils.dateComponentsFromString(timeString) == components)
  }

  func testAdjustTimeZonePlusMinusOneHour() {
    let GMTMinusEight = TimeZone.init(secondsFromGMT: (60 * 60) * 8)

    NSTimeZone.setDefaultTimeZone(GMTMinusEight)
    var calendar = Calendar.current
    calendar.timeZone = GMTMinusEight!

    let gmtDate = Date.dateShortHand(2015, month: 1, day: 1, hour: 0, minute: 0, second: 0)

    XCTAssert(gmtDate.adjustDateUsingTimeZone(TimeZone.init(abbreviation: "GMT")!) == LMSDateUtils.dateFromString("2015-01-01T00:00:00+0000"))
  }

}
