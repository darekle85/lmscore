//
//  NSData+LMSCRC32Tests.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 04.07.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//



import XCTest
@testable import LMSCore

class NSDataLMSCRC32Tests: XCTestCase {

  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    super.tearDown()
  }

  func testCRC32Generation() {
    let uuidString = "95D3008A-DF8B-4727-A9A9-90C96EE2330F"
    // 'expectedResult' - crc32 hash generated from '95D3008A-DF8B-4727-A9A9-90C96EE2330F' uuid using 'crc32' php function
    let expectedResult: UInt32 = 0x8403317d
    let data = uuidString.data(using: String.Encoding.utf8)
    let result = data?.crc32()
    XCTAssertEqual(result, expectedResult)
  }
}
