//
//  LMSABTestingGroupProviderTests.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 01.07.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable trailing_whitespace


import XCTest
@testable import LMSCore

private let domainName = "Testable"
private let groupInputKey = "ABTestingGroupInput"

class LMSABTestingGroupProviderTests: XCTestCase {
  
  var uuid: UUID? = UUID()
  let userDefaults = UserDefaults(suiteName: domainName)!
  var abTestingGroupProvider: LMSABTestingGroupProvider {
    return LMSABTestingGroupProvider(uuid: self.uuid, userDefaults: self.userDefaults)
  }
  
  override func setUp() {
    super.setUp()
    self.userDefaults.removePersistentDomain(forName: domainName)
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testUserWithProvidedUUIDShouldBeAssignedToBGroup() {
    self.uuid = UUID(uuidString: "95D3008A-DF8B-4727-A9A9-90C96EE2330F")
    
    let expectedGroup = LMSABTestingGroup.b
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, expectedGroup)
    
    // '0x8403317d' - crc32 hash generated from '95D3008A-DF8B-4727-A9A9-90C96EE2330F' uuid using 'crc32' php function
    let expectedGroupInput = 89 // 0x8403317d % 100
    XCTAssertEqual(self.abTestingGroupProvider.testGroupInput, expectedGroupInput)
  }
  
  func testUserWithEmotyUUIDShouldBeAssignedToAGroup() {
    self.uuid = nil
    
    let expectedGroup = LMSABTestingGroup.a
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, expectedGroup)
    
    let expectedGroupInput = 0
    XCTAssertEqual(self.abTestingGroupProvider.testGroupInput, expectedGroupInput)
  }
  
  func testTestGroupShouldPersistEvenIfUUIDHasChanged() {
    self.uuid = UUID()
    let testGroup = self.abTestingGroupProvider.testGroup
    self.uuid = UUID()
    XCTAssertEqual(testGroup, self.abTestingGroupProvider.testGroup)
  }
  
  func testGroupDistributionAccuracy() {
    var bucketA = 0
    var bucketB = 0
    
    let numberOfTrials = 10000
    let accuracy = 0.04 * Double(numberOfTrials) // 4%
    
    for _ in 0..<numberOfTrials {
      let randomUUID = UUID()
      self.uuid = randomUUID
      self.userDefaults.removePersistentDomain(forName: domainName)
      
      if self.abTestingGroupProvider.testGroup == .a {
        bucketA += 1
      } else {
        bucketB += 1
      }
    }
    
    print("bucketA: \(bucketA), bucketB: \(bucketB), accuracy +/- \(accuracy)")
    
    XCTAssertEqualWithAccuracy(Double(bucketA), Double(bucketB), accuracy: accuracy)
  }
  
  func testUserWith0GroupInputShouldBeAssignedToAGroup() {
    let testInput = 0
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.a)
  }

  func testUserWith45GroupInputShouldBeAssignedToAGroup() {
    let testInput = 45
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.a)
  }
  
  func testUserWith49GroupInputShouldBeAssignedToAGroup() {
    let testInput = 49
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.a)
  }
  
  func testUserWith50GroupInputShouldBeAssignedToBGroup() {
    let testInput = 50
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.b)
  }
  
  func testUserWith62GroupInputShouldBeAssignedToBGroup() {
    let testInput = 62
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.b)
  }
  
  func testUserWith99GroupInputShouldBeAssignedToBGroup() {
    let testInput = 99
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.b)
  }
  
  func testUserWithMalformedGroupInputShouldBeAssignedToAGroup() {
    let testInput = 100
    self.userDefaults.set(testInput, forKey: groupInputKey)
    XCTAssertEqual(self.abTestingGroupProvider.testGroup, LMSABTestingGroup.a)
  }
}
