//
//  UIColor+APHAdditionsTests.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 08.06.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable trailing_whitespace


import XCTest
@testable import LMSCore

class UIColorAPHAdditionsTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testCreationOfColorFromHexString() {
    
    let redColor = UIColor.red
    let semiTransparentRedColor = UIColor(red: 255.0, green: 0.0, blue: 0.0, alpha: 136.0/255.0)
    
    XCTAssertEqual(UIColor(hexString: "f00"), redColor)
    XCTAssertEqual(UIColor(hexString: "#f00"), redColor)
    XCTAssertEqual(UIColor(hexString: "ff0000"), redColor)
    XCTAssertEqual(UIColor(hexString: "#ff0000"), redColor)
    
    XCTAssertEqual(UIColor(hexString: "f008"), semiTransparentRedColor)
    XCTAssertEqual(UIColor(hexString: "#f008"), semiTransparentRedColor)
    XCTAssertEqual(UIColor(hexString: "ff000088"), semiTransparentRedColor)
    XCTAssertEqual(UIColor(hexString: "#ff000088"), semiTransparentRedColor)
    
    XCTAssertNil(UIColor(hexString: "#ff00008"))
    XCTAssertNil(UIColor(hexString: "#malfrmed"))
  }
  
  func testCreationOfHexStringFromColor() {
    
    let redColor = UIColor.red
    let semiTransparentRedColor = UIColor(red: 255.0, green: 0.0, blue: 0.0, alpha: 136.0/255.0)
    
    XCTAssertEqual(redColor.hexString(false), "#FF0000")
    XCTAssertEqual(redColor.description, "#FF0000FF")
    XCTAssertEqual(semiTransparentRedColor.debugDescription, "#FF000088")
  }
}
