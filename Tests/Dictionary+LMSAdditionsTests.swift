//
//  Dictionary+APHAdditionsTests.swift
//  LMSCore
//
//  Created by Paweł Kowalczyk on 08.06.2016.
//  Copyright © 2016 LifeMap Solutions. All rights reserved.
//

// swiftlint:disable trailing_whitespace


import XCTest
@testable import LMSCore

class DictionaryAPHAdditionsTests: XCTestCase {
  
  var dictionary: [String: Int] = [:]
  
  override func setUp() {
    super.setUp()
    self.dictionary = [ "A": 1, "B": 2, "C": 3, "D": 4 ]
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testUnionOfDictionariesUsingOperators() {
    let dictionary1 = [ "A": 1, "B": 2, "C": 3 ]
    let dictionary2 = [ "A": 1 ]
    let dictionary3 = [ "B": 2, "C": 3 ]
    let dictionary4 = [ "D": 4 ]
    
    XCTAssertEqual(self.dictionary, self.dictionary | dictionary1)
    XCTAssertEqual(self.dictionary, self.dictionary + dictionary1)
    
    XCTAssertEqual(self.dictionary, self.dictionary | dictionary2)
    XCTAssertEqual(self.dictionary, self.dictionary + dictionary2)
    
    XCTAssertEqual(self.dictionary, self.dictionary | dictionary3)
    XCTAssertEqual(self.dictionary, self.dictionary + dictionary3)
    
    XCTAssertEqual(self.dictionary, self.dictionary | dictionary4)
    XCTAssertEqual(self.dictionary, self.dictionary + dictionary4)
    
    XCTAssertEqual(dictionary2 | dictionary3, dictionary3 | dictionary2)
    XCTAssertEqual(dictionary2 | dictionary3 | dictionary4, self.dictionary)
    
    XCTAssertEqual(self.dictionary | [:], dictionary)
  }
  
  func testUnionOfDictionariesUsingMethod() {
    let dictionary1 = [ "A": 1, "B": 2, "C": 3 ]
    let dictionary2 = [ "A": 1 ]
    let dictionary3 = [ "B": 2, "C": 3 ]
    let dictionary4 = [ "D": 4 ]
    
    XCTAssertEqual(self.dictionary, self.dictionary.union(dictionary1))
    XCTAssertEqual(self.dictionary, self.dictionary.union(dictionary2))
    XCTAssertEqual(self.dictionary, self.dictionary.union(dictionary3))
    XCTAssertEqual(self.dictionary, self.dictionary.union(dictionary4))
    
    XCTAssertEqual(dictionary2.union(dictionary3), dictionary3.union(dictionary2))
    XCTAssertEqual(dictionary2.union(dictionary3).union(dictionary4), self.dictionary)
    
    XCTAssertEqual(self.dictionary.union([:]), dictionary)
  }
}
